# awesome_diy

> What I cannot create, I do not understand - Richard Feynman

A list of "Write your own software" projects.

## Programming Languages

- [Create Your Own Programming Language](http://createyourproglang.com)<br>
  Create your first programming language in Ruby.

- [Writing An Interpreter in Go](https://interpreterbook.com)<br>
  How to build an interpreter for a C-like programming language from scratch in Go.

- [Writing A Compiler In Go](https://compilerbook.com)<br>
  This is the sequel to [Writing An Interpreter In Go](https://interpreterbook.com). Build a compiler and a virtual machine.

- [Build Your Own Lisp](http://www.buildyourownlisp.com)<br>
  Learn C and build your own programming language in 1000 lines of code!

- [DIY Lang](https://github.com/kvalle/diy-lang)<br>
  A hands-on, test driven guide to implementing a simple Lisp-like language in Python.

- [(How to Write a (Lisp) Interpreter (in Python))](http://norvig.com/lispy.html)<br>
  How to implement computer language interpreters in general, and in particular to build an interpreter for most of the Scheme dialect of Lisp using Python 3.

- [Make a Lisp](https://github.com/kanaka/mal/blob/master/process/guide.md)<br>
  So you want to write a Lisp interpreter? Welcome!


- [Scheme from Scratch](http://peter.michaux.ca/articles/scheme-from-scratch-introduction)<br>
  Build up Scheme from C.

- [Write You A Scheme](https://www.wespiser.com/writings/wyas/home.html)<br>
  How to create a functional Scheme in Haskell.

## Databases

- [Let's Build a Simple Database](https://cstack.github.io/db_tutorial/)<br>
  Writing a sqlite clone from scratch in C

- [Implementing a Key-Value Store](http://codecapsule.com/2012/11/07/ikvs-implementing-a-key-value-store-table-of-contents/)<br>
  Develop a lightweight key-value store in understandable C++ code.

## Web Frameworks

- [Build Your Own Sinatra](https://getgood.at/build-your-own/sinatra)<br>
  A book about Rack, Sinatra, Rails, and all the frustrating things in-between. You will master Rack while building your own Sinatra like framework.

## Version Control

- [Git implemented in JavaScript](https://github.com/maryrosecook/gitlet)<br>
  Understand how Git works in 1000 lines of code.